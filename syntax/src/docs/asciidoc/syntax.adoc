= Sample Asciidoctor
:icons: font
:toc:
:source-highlighter: rouge

Writing documentation with Asciidoctor is awesome
sample for Fort X 2022.

== Tasks

* Presentation
* Demo

[TIP]
Try http://asciidoctor.org[Asciidoctor] and see if you like it

[source,java]
----
System.out.println("Some code");
----






















'''

=== Level 3

Some more text.

====== Level 4

We are in way deep.











== Lists

.Title for a list
* Level 1
** Level 1.1
* Level 2
** Level 2.1
*** Level 2.1.1

.Numbered list
. Level 1
. Level 2
.. Level 2.1

















== Simple formatting

Some sample formatting with *bold* and _italic_ text.
We can also combine them to a *_bolditalic_* text.

Or we want text to be `code`, which is also possible.





















== Image

.Happy
image::image.jpg[]
























== Links

http://asciidoctor.org

http://asciidoctor.org[Asciidoctor website]

:linkattrs:
http://asciidoctor.org[Asciidoctor website, window=_blank]

:hide-uri-scheme:
http://asciidoctor.org
























== Tables

.Sample table
|===
| Column 1 | Column 2

| Row 1-1
| Row 1-2

| Row 2-1
| Row 2-2

|===



[cols="1,1,2", options="header"] 
|===
| Column 1 
| Column 2
| Takes more space

| Row 1-1
| Row 1-2
| Row 1-3

| Row 2-1
| Row 2-2
| Row 2-3

|===











[format="csv", options="header"]
.CSV Table
|===
Column 1,Column 2
Row 1.1,Row 1.2
Row 2.1,Row 2.2
|===




































== Admonitions

TIP: Simple tip paragraph

WARNING: Do not do this!

NOTE: Remember this...

IMPORTANT: You must read this text.

CAUTION: Please watch out.


[TIP]
====
For more text than just one paragraph.

We can use a block and apply a type for the block.

In this case a tip.
====

























== Document attributes

:message: Hello Fort X 2022

We have defined an attribute with the name message and the value: {message}.

Asciidoctor comes with built-in attributes.
This document is generated with Asciidoctor {asciidoctor-version} for example. 
On {docdatetime}.

Links attibutes {linkattrs}























== Conditions

:!advanced: 

ifdef::advanced[]
NOTE: This will only be shown when advanced attribute is set
endif::advanced[]


ifndef::advanced[]
CAUTION: This will only be shown when advanced attribute is *not* set
endif::advanced[]



:release: 1

ifeval::[{release} > 0]
IMPORTANT: We have a release!
endif::[]


















== Blocks

....
This is a literal block
....

++++
<p>Include HTML <b>unprocessed</b> in the output. {message}
The backend must be HTML for this to work.
</p>
++++













.Java source
----
package fortx;

public class Sample {
    public static void main(String[] args) {
        final Sample sample = new Sample();
    }
}
----





.Java source
[source,java]
----
package fortx;

public class Sample {
    public static void main(String[] args) {
        final Sample sample = new Sample();
    }
}
----









.XML
[source,xml]
----
<?xml version="1.0"?>
<root>
    <element>Value</element>
</root>
----






.Java source
[source,java]
----
package fortx;  // <1>

public class Sample {
    public static void main(String[] args) {
        final Sample sample = new Sample();  // <2>
    }
}
----
<1> Package name reflects conference name.
<2> Create an instance of `Sample` class.



























== Includes


:javaSrc: ./

[source,java]
----
include::{javaSrc}/src/main/java/fortx/Sample.java[]
----









[source,java]
----
include::{javaSrc}/src/main/java/fortx/Sample.java[lines=4..6]
----






[source,java]
----
include::{javaSrc}/src/main/java/fortx/SampleBean.java[tag=main,indent=0]
----



















== Diagrams

[ditaa,asciidoctor-diagram-process]
....
                   +-------------+
                   | Asciidoctor |-------+
                   |   diagram   |       |
                   +-------------+       | PNG out
                       ^                 |
                       | ditaa in        |
                       |                 v
 +--------+   +--------+----+    /---------------\
 |        |---+ Asciidoctor +--->|               |
 |  Text  |   +-------------+    |   Beautiful   |
 |Document|   |   !magic!   |    |    Output     |
 |     {d}|   |             |    |               |
 +---+----+   +-------------+    \---------------/
     :                                   ^
     |          Lots of work             |
     +-----------------------------------+
....

[plantuml]
....
@startuml
package "Some Group" {
  HTTP - [First Component]
  [Another Component]
}

node "Other Groups" {
  FTP - [Second Component]
  [First Component] --> FTP
}

cloud {
  [Example 1]
}

database "MySql" {
  folder "This is my folder" {
    [Folder 3]
  }
  frame "Foo" {
    [Frame 4]
  }
}

[Another Component] --> [Example 1]
[Example 1] --> [Folder 3]
[Folder 3] --> [Frame 4]
@enduml
....


