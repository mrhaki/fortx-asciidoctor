package fortx;

import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.InlineMacroProcessor;
import org.asciidoctor.extension.Name;

import java.util.HashMap;
import java.util.Map;

@Name("twitter")
public class TwitterMacro extends InlineMacroProcessor {
    @Override
    public Object process(final ContentNode parent, final String target, final Map<String, Object> attributes) {
        String twitterUrl = String.format("https://www.twitter.com/%s", target);
        Map<String, Object> options = new HashMap<>();
        options.put("type", ":link");
        options.put("target", twitterUrl);
        return createPhraseNode(parent, "anchor", target, attributes, options);
    }
}
