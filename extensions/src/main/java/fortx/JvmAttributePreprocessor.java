package fortx;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Preprocessor;
import org.asciidoctor.extension.PreprocessorReader;

public class JvmAttributePreprocessor extends Preprocessor {
    @Override
    public void process(final Document document, final PreprocessorReader reader) {
        String jvmVersion = System.getProperty("java.version");
        document.setAttribute("java-version", jvmVersion, true);

        String userName = System.getProperty("user.name");
        document.setAttribute("username", userName, false);
    }
}
