package fortx;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

public class TwitterMacroExtension implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        var javaExtensionRegistry = asciidoctor.javaExtensionRegistry();
        javaExtensionRegistry.inlineMacro(TwitterMacro.class);
    }
}
