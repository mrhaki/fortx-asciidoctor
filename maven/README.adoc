= Micronaut 3.5.2 Documentation

* https://docs.micronaut.io/3.5.2/guide/index.html[User Guide]
* https://docs.micronaut.io/3.5.2/api/index.html[API Reference]
* https://docs.micronaut.io/3.5.2/guide/configurationreference.html[Configuration Reference]
* https://guides.micronaut.io/index.html[Micronaut Guides]

== Feature http-client documentation

* https://docs.micronaut.io/latest/guide/index.html#httpClient[Micronaut HTTP Client documentation]


