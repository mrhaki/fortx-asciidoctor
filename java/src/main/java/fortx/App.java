package fortx;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;

public class App {
    public static void main(String[] args) {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            String adoc = asciidoctor.convert("Writing *AsciiDoc* is _easy_!", 
                                              Options.builder().build());
            System.out.println(adoc);
        }
    }
}
